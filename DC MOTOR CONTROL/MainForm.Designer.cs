﻿namespace DC_MOTOR_CONTROL
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.zed = new ZedGraph.ZedGraphControl();
            this.label7 = new System.Windows.Forms.Label();
            this.txtNumOfPointPerAXis = new System.Windows.Forms.TextBox();
            this.txtNumOfPlots = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ckbHorizontalLine = new System.Windows.Forms.CheckBox();
            this.txtScaler = new System.Windows.Forms.TextBox();
            this.ckbAuto = new System.Windows.Forms.CheckBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.Pbmode = new System.Windows.Forms.Button();
            this.Pbclear = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.txtstatuslb = new System.Windows.Forms.ToolStripStatusLabel();
            this.label14 = new System.Windows.Forms.Label();
            this.@__serialPort = new System.IO.Ports.SerialPort(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.txtDataReceived = new System.Windows.Forms.TextBox();
            this.cmbPortName = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbBaudRate = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbDataBits = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbParityBit = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbStopBits = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnConnect_Label = new System.Windows.Forms.Button();
            this.Pbexit = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chk_TerminalOnly = new System.Windows.Forms.CheckBox();
            this.pid_parameters_grp = new System.Windows.Forms.GroupBox();
            this.txt_kp = new System.Windows.Forms.TextBox();
            this.txt_ki = new System.Windows.Forms.TextBox();
            this.txt_kd = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btn_SendPID = new System.Windows.Forms.Button();
            this.button_Start = new System.Windows.Forms.Button();
            this.button_End = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.pid_parameters_grp.SuspendLayout();
            this.SuspendLayout();
            // 
            // zed
            // 
            this.zed.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.zed.BackColor = System.Drawing.SystemColors.Control;
            this.zed.Location = new System.Drawing.Point(12, 52);
            this.zed.Margin = new System.Windows.Forms.Padding(5);
            this.zed.Name = "zed";
            this.zed.ScrollGrace = 0D;
            this.zed.ScrollMaxX = 0D;
            this.zed.ScrollMaxY = 0D;
            this.zed.ScrollMaxY2 = 0D;
            this.zed.ScrollMinX = 0D;
            this.zed.ScrollMinY = 0D;
            this.zed.ScrollMinY2 = 0D;
            this.zed.Size = new System.Drawing.Size(759, 439);
            this.zed.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(359, 9);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 29);
            this.label7.TabIndex = 15;
            this.label7.Text = "Graph";
            // 
            // txtNumOfPointPerAXis
            // 
            this.txtNumOfPointPerAXis.Location = new System.Drawing.Point(144, 28);
            this.txtNumOfPointPerAXis.Margin = new System.Windows.Forms.Padding(4);
            this.txtNumOfPointPerAXis.Name = "txtNumOfPointPerAXis";
            this.txtNumOfPointPerAXis.Size = new System.Drawing.Size(108, 22);
            this.txtNumOfPointPerAXis.TabIndex = 18;
            this.txtNumOfPointPerAXis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtNumOfPlots
            // 
            this.txtNumOfPlots.Location = new System.Drawing.Point(144, 94);
            this.txtNumOfPlots.Margin = new System.Windows.Forms.Padding(4);
            this.txtNumOfPlots.Name = "txtNumOfPlots";
            this.txtNumOfPlots.Size = new System.Drawing.Size(108, 22);
            this.txtNumOfPlots.TabIndex = 20;
            this.txtNumOfPlots.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(7, 29);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(130, 17);
            this.label10.TabIndex = 23;
            this.label10.Text = "MaxNumOfPoints";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(7, 98);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(93, 17);
            this.label12.TabIndex = 25;
            this.label12.Text = "NumOfPlots";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.ckbHorizontalLine);
            this.groupBox2.Controls.Add(this.txtScaler);
            this.groupBox2.Controls.Add(this.ckbAuto);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtNumOfPlots);
            this.groupBox2.Controls.Add(this.txtNumOfPointPerAXis);
            this.groupBox2.ForeColor = System.Drawing.Color.Blue;
            this.groupBox2.Location = new System.Drawing.Point(229, 512);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(260, 155);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Parameters";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(7, 133);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 17);
            this.label6.TabIndex = 30;
            this.label6.Text = "Scaler";
            // 
            // ckbHorizontalLine
            // 
            this.ckbHorizontalLine.AutoSize = true;
            this.ckbHorizontalLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.ckbHorizontalLine.ForeColor = System.Drawing.Color.Crimson;
            this.ckbHorizontalLine.Location = new System.Drawing.Point(8, 62);
            this.ckbHorizontalLine.Name = "ckbHorizontalLine";
            this.ckbHorizontalLine.Size = new System.Drawing.Size(140, 21);
            this.ckbHorizontalLine.TabIndex = 27;
            this.ckbHorizontalLine.Text = "Horizontal Line";
            this.ckbHorizontalLine.UseVisualStyleBackColor = true;
            this.ckbHorizontalLine.CheckedChanged += new System.EventHandler(this.HorizontalLine_Changed);
            // 
            // txtScaler
            // 
            this.txtScaler.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtScaler.Location = new System.Drawing.Point(144, 126);
            this.txtScaler.Name = "txtScaler";
            this.txtScaler.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtScaler.Size = new System.Drawing.Size(108, 22);
            this.txtScaler.TabIndex = 29;
            this.txtScaler.Text = "1000";
            this.txtScaler.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ckbAuto
            // 
            this.ckbAuto.AutoSize = true;
            this.ckbAuto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.ckbAuto.Location = new System.Drawing.Point(167, 62);
            this.ckbAuto.Name = "ckbAuto";
            this.ckbAuto.Size = new System.Drawing.Size(63, 21);
            this.ckbAuto.TabIndex = 26;
            this.ckbAuto.Text = "Auto";
            this.ckbAuto.UseVisualStyleBackColor = true;
            this.ckbAuto.CheckedChanged += new System.EventHandler(this.Checkbox_Auto_Changed);
            // 
            // btnStart
            // 
            this.btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.ForeColor = System.Drawing.Color.Green;
            this.btnStart.Location = new System.Drawing.Point(21, 27);
            this.btnStart.Margin = new System.Windows.Forms.Padding(4);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(100, 103);
            this.btnStart.TabIndex = 29;
            this.btnStart.Text = "START";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // Pbmode
            // 
            this.Pbmode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Pbmode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pbmode.Location = new System.Drawing.Point(159, 27);
            this.Pbmode.Margin = new System.Windows.Forms.Padding(4);
            this.Pbmode.Name = "Pbmode";
            this.Pbmode.Size = new System.Drawing.Size(100, 43);
            this.Pbmode.TabIndex = 30;
            this.Pbmode.Text = "COMPACT";
            this.Pbmode.UseVisualStyleBackColor = true;
            this.Pbmode.Click += new System.EventHandler(this.BtnMode_Click);
            // 
            // Pbclear
            // 
            this.Pbclear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Pbclear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pbclear.ForeColor = System.Drawing.Color.Red;
            this.Pbclear.Location = new System.Drawing.Point(159, 89);
            this.Pbclear.Margin = new System.Windows.Forms.Padding(4);
            this.Pbclear.Name = "Pbclear";
            this.Pbclear.Size = new System.Drawing.Size(100, 42);
            this.Pbclear.TabIndex = 31;
            this.Pbclear.Text = "CLEAR";
            this.Pbclear.UseVisualStyleBackColor = true;
            this.Pbclear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.Pbclear);
            this.groupBox4.Controls.Add(this.Pbmode);
            this.groupBox4.Controls.Add(this.btnStart);
            this.groupBox4.ForeColor = System.Drawing.Color.Blue;
            this.groupBox4.Location = new System.Drawing.Point(497, 511);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(273, 149);
            this.groupBox4.TabIndex = 32;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "CTRL_GRAPH";
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtstatuslb});
            this.statusStrip1.Location = new System.Drawing.Point(0, 684);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1301, 22);
            this.statusStrip1.TabIndex = 34;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // txtstatuslb
            // 
            this.txtstatuslb.ForeColor = System.Drawing.Color.Red;
            this.txtstatuslb.Name = "txtstatuslb";
            this.txtstatuslb.Size = new System.Drawing.Size(0, 17);
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Image = ((System.Drawing.Image)(resources.GetObject("label14.Image")));
            this.label14.Location = new System.Drawing.Point(794, 517);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(143, 138);
            this.label14.TabIndex = 36;
            this.label14.Text = " TTĐTTN-CLC\r\n     ĐẠI HỌC\r\n  BÁCH KHOA\r\n      HÀ NỘI\r\n   CTES-HUST\r\nwb:hust.edu.v" +
    "n";
            // 
            // __serialPort
            // 
            this.@__serialPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.DataReceivedFromComnHandler);
            // 
            // timer1
            // 
            this.timer1.Interval = 25;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // txtDataReceived
            // 
            this.txtDataReceived.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDataReceived.Location = new System.Drawing.Point(952, 52);
            this.txtDataReceived.Multiline = true;
            this.txtDataReceived.Name = "txtDataReceived";
            this.txtDataReceived.ReadOnly = true;
            this.txtDataReceived.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtDataReceived.Size = new System.Drawing.Size(337, 603);
            this.txtDataReceived.TabIndex = 37;
            // 
            // cmbPortName
            // 
            this.cmbPortName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbPortName.FormattingEnabled = true;
            this.cmbPortName.Location = new System.Drawing.Point(12, 49);
            this.cmbPortName.Margin = new System.Windows.Forms.Padding(4);
            this.cmbPortName.Name = "cmbPortName";
            this.cmbPortName.Size = new System.Drawing.Size(133, 24);
            this.cmbPortName.TabIndex = 0;
            this.cmbPortName.SelectedIndexChanged += new System.EventHandler(this.PortName_SelectChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Port Name";
            // 
            // cmbBaudRate
            // 
            this.cmbBaudRate.FormattingEnabled = true;
            this.cmbBaudRate.Location = new System.Drawing.Point(13, 98);
            this.cmbBaudRate.Margin = new System.Windows.Forms.Padding(4);
            this.cmbBaudRate.Name = "cmbBaudRate";
            this.cmbBaudRate.Size = new System.Drawing.Size(133, 24);
            this.cmbBaudRate.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 76);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Baud Rate";
            // 
            // cmbDataBits
            // 
            this.cmbDataBits.FormattingEnabled = true;
            this.cmbDataBits.Location = new System.Drawing.Point(13, 153);
            this.cmbDataBits.Margin = new System.Windows.Forms.Padding(4);
            this.cmbDataBits.Name = "cmbDataBits";
            this.cmbDataBits.Size = new System.Drawing.Size(133, 24);
            this.cmbDataBits.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 130);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Data Sizes";
            // 
            // cmbParityBit
            // 
            this.cmbParityBit.FormattingEnabled = true;
            this.cmbParityBit.Location = new System.Drawing.Point(13, 206);
            this.cmbParityBit.Margin = new System.Windows.Forms.Padding(4);
            this.cmbParityBit.Name = "cmbParityBit";
            this.cmbParityBit.Size = new System.Drawing.Size(133, 24);
            this.cmbParityBit.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 183);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Parity";
            // 
            // cmbStopBits
            // 
            this.cmbStopBits.FormattingEnabled = true;
            this.cmbStopBits.Location = new System.Drawing.Point(13, 257);
            this.cmbStopBits.Margin = new System.Windows.Forms.Padding(4);
            this.cmbStopBits.Name = "cmbStopBits";
            this.cmbStopBits.Size = new System.Drawing.Size(133, 24);
            this.cmbStopBits.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 234);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Stop bits";
            // 
            // btnConnect_Label
            // 
            this.btnConnect_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect_Label.ForeColor = System.Drawing.Color.Green;
            this.btnConnect_Label.Location = new System.Drawing.Point(13, 310);
            this.btnConnect_Label.Margin = new System.Windows.Forms.Padding(4);
            this.btnConnect_Label.Name = "btnConnect_Label";
            this.btnConnect_Label.Size = new System.Drawing.Size(135, 57);
            this.btnConnect_Label.TabIndex = 10;
            this.btnConnect_Label.Text = "CONNECT";
            this.btnConnect_Label.UseVisualStyleBackColor = true;
            this.btnConnect_Label.Click += new System.EventHandler(this.BtnConnect_Click);
            // 
            // Pbexit
            // 
            this.Pbexit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pbexit.ForeColor = System.Drawing.Color.Red;
            this.Pbexit.Location = new System.Drawing.Point(13, 374);
            this.Pbexit.Margin = new System.Windows.Forms.Padding(4);
            this.Pbexit.Name = "Pbexit";
            this.Pbexit.Size = new System.Drawing.Size(135, 30);
            this.Pbexit.TabIndex = 11;
            this.Pbexit.Text = "EXIT";
            this.Pbexit.UseVisualStyleBackColor = true;
            this.Pbexit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.Pbexit);
            this.groupBox1.Controls.Add(this.btnConnect_Label);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cmbStopBits);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cmbParityBit);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cmbDataBits);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cmbBaudRate);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmbPortName);
            this.groupBox1.ForeColor = System.Drawing.Color.Blue;
            this.groupBox1.Location = new System.Drawing.Point(788, 41);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(157, 416);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Serial Port";
            // 
            // chk_TerminalOnly
            // 
            this.chk_TerminalOnly.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chk_TerminalOnly.AutoSize = true;
            this.chk_TerminalOnly.Location = new System.Drawing.Point(801, 470);
            this.chk_TerminalOnly.Name = "chk_TerminalOnly";
            this.chk_TerminalOnly.Size = new System.Drawing.Size(118, 21);
            this.chk_TerminalOnly.TabIndex = 38;
            this.chk_TerminalOnly.Text = "Terminal Only";
            this.chk_TerminalOnly.UseVisualStyleBackColor = true;
            this.chk_TerminalOnly.CheckedChanged += new System.EventHandler(this.Terminal_Only_Handler);
            // 
            // pid_parameters_grp
            // 
            this.pid_parameters_grp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pid_parameters_grp.Controls.Add(this.button_End);
            this.pid_parameters_grp.Controls.Add(this.button_Start);
            this.pid_parameters_grp.Controls.Add(this.btn_SendPID);
            this.pid_parameters_grp.Controls.Add(this.label11);
            this.pid_parameters_grp.Controls.Add(this.label9);
            this.pid_parameters_grp.Controls.Add(this.label8);
            this.pid_parameters_grp.Controls.Add(this.txt_kd);
            this.pid_parameters_grp.Controls.Add(this.txt_ki);
            this.pid_parameters_grp.Controls.Add(this.txt_kp);
            this.pid_parameters_grp.Location = new System.Drawing.Point(18, 512);
            this.pid_parameters_grp.Name = "pid_parameters_grp";
            this.pid_parameters_grp.Size = new System.Drawing.Size(200, 169);
            this.pid_parameters_grp.TabIndex = 39;
            this.pid_parameters_grp.TabStop = false;
            this.pid_parameters_grp.Text = "PID Controls";
            // 
            // txt_kp
            // 
            this.txt_kp.Location = new System.Drawing.Point(89, 24);
            this.txt_kp.Name = "txt_kp";
            this.txt_kp.Size = new System.Drawing.Size(100, 22);
            this.txt_kp.TabIndex = 0;
            this.txt_kp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_ki
            // 
            this.txt_ki.Location = new System.Drawing.Point(89, 57);
            this.txt_ki.Name = "txt_ki";
            this.txt_ki.Size = new System.Drawing.Size(100, 22);
            this.txt_ki.TabIndex = 1;
            this.txt_ki.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_kd
            // 
            this.txt_kd.Location = new System.Drawing.Point(89, 91);
            this.txt_kd.Name = "txt_kd";
            this.txt_kd.Size = new System.Drawing.Size(100, 22);
            this.txt_kd.TabIndex = 2;
            this.txt_kd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 17);
            this.label8.TabIndex = 3;
            this.label8.Text = "kP";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(18, 17);
            this.label9.TabIndex = 4;
            this.label9.Text = "kI";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 90);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 17);
            this.label11.TabIndex = 5;
            this.label11.Text = "kD";
            // 
            // btn_SendPID
            // 
            this.btn_SendPID.Location = new System.Drawing.Point(67, 126);
            this.btn_SendPID.Name = "btn_SendPID";
            this.btn_SendPID.Size = new System.Drawing.Size(64, 29);
            this.btn_SendPID.TabIndex = 6;
            this.btn_SendPID.Text = "Send";
            this.btn_SendPID.UseVisualStyleBackColor = true;
            this.btn_SendPID.Click += new System.EventHandler(this.Send_PID_Handle);
            // 
            // button_Start
            // 
            this.button_Start.Location = new System.Drawing.Point(6, 127);
            this.button_Start.Name = "button_Start";
            this.button_Start.Size = new System.Drawing.Size(55, 28);
            this.button_Start.TabIndex = 7;
            this.button_Start.Text = "Start";
            this.button_Start.UseVisualStyleBackColor = true;
            this.button_Start.Click += new System.EventHandler(this.Start_Transmitt);
            // 
            // button_End
            // 
            this.button_End.Location = new System.Drawing.Point(137, 126);
            this.button_End.Name = "button_End";
            this.button_End.Size = new System.Drawing.Size(52, 29);
            this.button_End.TabIndex = 8;
            this.button_End.Text = "End";
            this.button_End.UseVisualStyleBackColor = true;
            this.button_End.Click += new System.EventHandler(this.Stop_Transmitt);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1301, 706);
            this.Controls.Add(this.pid_parameters_grp);
            this.Controls.Add(this.chk_TerminalOnly);
            this.Controls.Add(this.txtDataReceived);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.zed);
            this.Controls.Add(this.groupBox1);
            this.ForeColor = System.Drawing.Color.Blue;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.Text = "DC MOTOR CONTROL";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.form1_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pid_parameters_grp.ResumeLayout(false);
            this.pid_parameters_grp.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private ZedGraph.ZedGraphControl zed;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtNumOfPointPerAXis;
        private System.Windows.Forms.TextBox txtNumOfPlots;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button Pbmode;
        private System.Windows.Forms.Button Pbclear;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Label label14;
        private System.IO.Ports.SerialPort __serialPort;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripStatusLabel txtstatuslb;
        private System.Windows.Forms.TextBox txtDataReceived;
        private System.Windows.Forms.ComboBox cmbPortName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbBaudRate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbDataBits;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbParityBit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbStopBits;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnConnect_Label;
        private System.Windows.Forms.Button Pbexit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox ckbAuto;
        private System.Windows.Forms.CheckBox ckbHorizontalLine;
        private System.Windows.Forms.CheckBox chk_TerminalOnly;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtScaler;
        private System.Windows.Forms.GroupBox pid_parameters_grp;
        private System.Windows.Forms.Button btn_SendPID;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_kd;
        private System.Windows.Forms.TextBox txt_ki;
        private System.Windows.Forms.TextBox txt_kp;
        private System.Windows.Forms.Button button_End;
        private System.Windows.Forms.Button button_Start;
    }
}

