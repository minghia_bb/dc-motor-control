﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using ZedGraph;

namespace DC_MOTOR_CONTROL
{
    public partial class MainForm : Form
    {
        int _OneMode = 1;                        // onemode-control start, stop
        const int xMax = 100;                    // Max unit of x axis
        int timerinterval = 200;                 // in ms, Timer
        bool bStart = false;                     // Whether Graph will be drawn
        int maxNumOfPointPerAxis = 200;          // Max number of points each axis will hold
        //int NumOfPlots = 1;                      // number of plots will be shown in Graph
        double _nX_Index = 0;                    // current value of x-axis
        const int NumOfLines = 150;              // number of lines in Terminal Panel 
        int _indLine = 0;                        // line index indicates that the number of lines reaches to maximum line
        double __unit = 1;                       // DEG/RAG
        double __scaler = 1000;					 // conversion from INT to double
        private delegate void DlDisplay(string s, string s2);
        bool DataMode = true;                    // Data from serial is numeric
        public MainForm()
        {
            InitializeComponent();
            //******************************************************************************
            string[] ports = SerialPort.GetPortNames();
            cmbPortName.Items.AddRange(ports);
            // BaudRate
            string[] BaudRate = { "1200", "2400", "4800", "9600", "19200", "38400", "57600", "115200" };
            cmbBaudRate.Items.AddRange(BaudRate);
            // DataBits
            string[] Databits = { "6", "7", "8" };
            cmbDataBits.Items.AddRange(Databits);
            //Cho Parity
            string[] Parity = { "None", "Odd", "Even" };
            cmbParityBit.Items.AddRange(Parity);
            //Cho Stop bit
            string[] stopbit = { "0", "1", "2", "1.5" };
            cmbStopBits.Items.AddRange(stopbit);
        }
        /// <summary>
        /// Mainform Load 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void form1_Load(object sender, EventArgs e)
        {
            if (cmbPortName.Items.Count > 0)
            {
                cmbPortName.SelectedIndex = 0;
            }
            //cmbBaudRate.SelectedIndex = 3; /* 9600 */
            cmbBaudRate.SelectedIndex = 5;   /* 38400 */
            cmbDataBits.SelectedIndex = 2;   /* 8 */
            cmbParityBit.SelectedIndex = 0;  /* None */
            cmbStopBits.SelectedIndex = 1;   /* One */
            txtstatuslb.Text = "Disconected";
            timer1.Interval = timerinterval;
            //*************************************************************************************
            //ZEDGRAPH
            ResetGraph();

            txtNumOfPointPerAXis.Text = maxNumOfPointPerAxis.ToString();
            txtNumOfPlots.Text = NumOfPlots.ToString();

            ckbAuto.Enabled = false;
            txtNumOfPlots.Enabled = !ckbAuto.Checked;

            // PID params
            txt_kp.Text = "1";
            txt_ki.Text = "0";
            txt_kd.Text = "0";
            btn_SendPID.Enabled = false;
            button_Start.Enabled = false;
            button_End.Enabled = false;
        }

        #region Handler
        /// <summary>
        /// Refresh Available PortName
        /// </summary>
        private void PortName_Available_Update()
        {
            string[] ports = SerialPort.GetPortNames();
            if (ports.Length > 0)
            {
                for (int i = 0; i < ports.Length; i++)
                {
                    if (cmbPortName.Items.Count == 0 || !cmbPortName.Items.Contains(ports[i]))
                    {
                        cmbPortName.Items.Add(ports[i]);
                    }
                }
            }
            else
            {
                cmbPortName.SelectedItem = null;
                cmbPortName.Items.Clear();
            }
        }
        /// <summary>
        /// Status of Serial Port is connected or not
        /// </summary>
        bool isSerialPortConnected = false;
        /// <summary>
        /// CONNECT Button Clicked Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnConnect_Click(object sender, EventArgs e)
        {
            // Now is Offline, want to be Online
            if (!isSerialPortConnected)
            {
                PortName_Available_Update();
                if (cmbPortName.Items.Count < 1)
                {
                    MessageBox.Show("There isn't any available port now!\r\nPlease plug the cable!",
                                    "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    __serialPort.PortName = cmbPortName.SelectedItem.ToString();
                    __serialPort.BaudRate = Convert.ToInt32(cmbBaudRate.SelectedItem.ToString()); ;
                    __serialPort.DataBits = 6 + (cmbDataBits.SelectedIndex);
                    __serialPort.Parity = (Parity)(cmbParityBit.SelectedIndex);
                    __serialPort.StopBits = (StopBits)(cmbStopBits.SelectedIndex);
                    try
                    {
                        __serialPort.Open();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "_Error_", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        PortName_Available_Update();
                        return;
                    }
                }
            }
            else
            {
                // disconnect the serial port
                try
                {
                    __serialPort.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "_Error_", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            isSerialPortConnected = !isSerialPortConnected;
            btnConnect_Label.Text = isSerialPortConnected ? "DISCONNECT" : "CONNECT";
            txtstatuslb.Text = isSerialPortConnected ? "Connected to : " + cmbPortName.Text : "Disconected!";
            cmbPortName.Enabled = !isSerialPortConnected;
            cmbBaudRate.Enabled = !isSerialPortConnected;
            cmbDataBits.Enabled = !isSerialPortConnected;
            cmbParityBit.Enabled = !isSerialPortConnected;
            cmbStopBits.Enabled = !isSerialPortConnected;

            if (txtScaler.Text != "")
            {
                int temp;
                if (int.TryParse(txtScaler.Text, out temp) && temp > 0)
                {
                    __scaler = temp;
                }
            }
            txtScaler.Text = ((int)(__scaler)).ToString(); // Update to GUI
            txtScaler.Enabled = !isSerialPortConnected;

            button_Start.Enabled = isSerialPortConnected;
            //button_End.Enabled = !button_Start.Enabled;
            //btn_SendPID.Enabled = !button_Start.Enabled;
        }
        /// <summary>
        /// ComboBox PortName SelectedChanged Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PortName_SelectChanged(object sender, EventArgs e)
        {
            PortName_Available_Update();
        }
        private void BtnExit_Click(object sender, EventArgs e)
        {
            if (__serialPort.IsOpen)
            {
                try
                {
                    __serialPort.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "_Error_", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            Close();
        }
        /// <summary>
        /// Software Timer Tick Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            Draw();
            int numPlots = zed.GraphPane.CurveList.Count;
        }
        /// <summary>
        /// START button Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnStart_Click(object sender, EventArgs e)
        {
            int temp;
            bStart = !bStart;
            timer1.Enabled = bStart;
            btnStart.Text = bStart ? "STOP" : "START";
            if (txtNumOfPlots.Text != "")
            {
                int.TryParse(txtNumOfPlots.Text, out NumOfPlots);
            }
            txtNumOfPlots.Text = NumOfPlots.ToString();
            maxNumOfPointPerAxis = 200; // default
            if (txtNumOfPointPerAXis.Text != "")
            {
                if (int.TryParse(txtNumOfPointPerAXis.Text, out temp) && temp > 0)
                {
                    maxNumOfPointPerAxis = temp;
                }
            }
            txtNumOfPointPerAXis.Text = maxNumOfPointPerAxis.ToString(); // Update to GUi
            txtNumOfPointPerAXis.Enabled = !bStart;
            ckbAuto.Enabled = !bStart;
            txtNumOfPlots.Enabled = !bStart && !ckbAuto.Checked;
        }
        private void BtnMode_Click(object sender, EventArgs e)
        {
            if (Pbmode.Text == "SROLL") // COMPACT --> SROLL
            {
                _OneMode = 1; // Im in SROLL mode now
                Pbmode.Text = "COMPACT";
            }
            else // SROLL --> COMPACT
            {
                _OneMode = 0; // Im in COMPACT mode now
                Pbmode.Text = "SROLL";
            }
        }
        //
        private void AddPoint_Click(object sender, EventArgs e)
        {
            double data = 10 * Math.Sin(_nX_Index * Math.PI / 20);
            int[] datas = new int[1];
            datas[0] = (int)(data * __scaler / __unit);
            UpdateData(datas);
            txtDataReceived.Text = "";
            for (int i = 0; i < zed.GraphPane.CurveList[0].NPts; i++)
            {
                double x = zed.GraphPane.CurveList[0].Points[i].X;
                double y = zed.GraphPane.CurveList[0].Points[i].Y;
                txtDataReceived.Text += String.Format("[{0:0.0},{1:0.0}]\r\n", x, y);
            }
            Draw();
        }

        private void RemovePoint_Click(object sender, EventArgs e)
        {
            if (zed.GraphPane.CurveList.Count < 1) { return; }
            if (zed.GraphPane.CurveList[0].NPts < 1) { return; }
            for (int i = 0; i < zed.GraphPane.CurveList.Count; i++)
            {
                zed.GraphPane.CurveList[i].RemovePoint(0);
            }
            txtDataReceived.Text = "";
            for (int i = 0; i < zed.GraphPane.CurveList[0].NPts; i++)
            {
                double x = zed.GraphPane.CurveList[0].Points[i].X;
                double y = zed.GraphPane.CurveList[0].Points[i].Y;
                txtDataReceived.Text += String.Format("[{0:0.0},{1:0.0}]\r\n", x, y);
            }
            Draw();
        }
        private void BtnClear_Click(object sender, EventArgs e)
        {
            ResetGraph();
            zed.AxisChange();
            zed.Invalidate();
        }
        private void Checkbox_Auto_Changed(object sender, EventArgs e)
        {
            txtNumOfPlots.Enabled = !ckbAuto.Checked;
        }
        private void HorizontalLine_Changed(object sender, EventArgs e)
        {
            zed.GraphPane.YAxis.MajorGrid.IsZeroLine = ckbHorizontalLine.Checked;
        }
        #endregion
        private void ResetGraph()
        {
            GraphPane mypane = zed.GraphPane;
            mypane.Title.Text = "MOTORCONTROL";             //ten do thi
            mypane.XAxis.Title.Text = "Time , Second";      //ten truc x
            mypane.YAxis.Title.Text = "Angle , Dec ";       //ten truc y
            /////set kich thuoc do thi truc x
            mypane.XAxis.Scale.Min = 0;                     // diem bat dau
            mypane.XAxis.Scale.Max = xMax;                  // diem ket thuc
            mypane.XAxis.Scale.MinorStep = 2;               // do chia nho
            mypane.XAxis.Scale.MajorStep = 5;               // do chia lon
            for (int i = 0; i < zed.GraphPane.CurveList.Count; i++)
            {
                zed.GraphPane.CurveList[i].Clear();
            }
            zed.GraphPane.YAxis.MajorGrid.IsZeroLine = false;
            zed.AxisChange();                               // thay doi do this
        }
        // number of bytes each element
        int numberOfbytes = 4;
        // number of elements each row
        int NumOfPlots = 1;
        // Frame of data = data + CRC, CRC = data1 ^ data2...
        private void DataReceivedFromComnHandler(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            string receivedTxt = "";
            if (!DataMode)
            {
                receivedTxt = __serialPort.ReadLine() + "\r\n";
                DataReceived_Display(receivedTxt, NumOfPlots.ToString());
                return;
            }
            int n_bytes = 0, n_rows = 1;
            int frame_data = numberOfbytes * NumOfPlots + 1;
            try
            {
                if (__serialPort.BytesToRead > frame_data + 1)
                {
                    //n_rows = serialPort1.BytesToRead / frame_data;
                    n_bytes = n_rows * frame_data;
                    byte[] buffer = new byte[n_bytes];
                    __serialPort.Read(buffer, 0, n_bytes);
                    receivedTxt = "";
                    for (int i = 0; i < n_rows; i++)
                    {
                        byte crc = 0xFF;
                        for (int j = 0; j < frame_data - 1; j++)
                        {
                            try
                            {
                                crc ^= buffer[i * frame_data + j];
                            }
                            catch (Exception ex) { MessageBox.Show(ex.Message, "I am heere"); }
                        }
                        // check CRC
                        try
                        {
                            if (crc == buffer[(i + 1) * frame_data - 1])
                            {
                                receivedTxt = "";
                                int[] dataReceived = new int[NumOfPlots];
                                for (int j = 0; j < NumOfPlots; j++)
                                {
                                    int value = 0;
                                    dataReceived[j] = 0;
                                    for (int k = 0; k < numberOfbytes; k++)
                                    {
                                        //receivedTxt += buffer[i * frame_data + j * NumOfPlots + k] + ",";
                                        try
                                        {
                                            value += ((buffer[i * frame_data + j * NumOfPlots + k]) << (k * 8));
                                        }
                                        catch (Exception ex) { MessageBox.Show(ex.Message, "I am heere 2"); }
                                    }
                                    dataReceived[j] = value;

                                    //receivedTxt += value + ", ";
                                }
                                //receivedTxt += "..." + crc + "?" + buffer[(i + 1) * frame_data - 1] + "\r\n";
                                //receivedTxt += "\n";

                                receivedTxt = UpdateData(dataReceived);
                                DataReceived_Display(receivedTxt, NumOfPlots.ToString());
                            }
                            else
                            {
                                __serialPort.ReadExisting();
                                //__serialPort.ReadByte();
                                receivedTxt = "CRC failed \r\n";
                                //receivedTxt = "";
                                //for (int ii = 0; ii < frame_data - 1; ii++)
                                //{
                                //    receivedTxt += buffer[i * frame_data + ii] + ",";
                                //}
                                //receivedTxt += buffer[(i + 1) * frame_data - 1] + "=?" + crc + "\r\n";
                                DataReceived_Display(receivedTxt, NumOfPlots.ToString());
                            }
                        }
                        catch (Exception ex)
                        { MessageBox.Show(ex.Message, "I am heere 3"); }
                    }
                }
            }
            catch (Exception ex)
            {
                DataReceived_Display(ex.Message + "\r\n", NumOfPlots.ToString());
                MessageBox.Show(ex.Message, "Error while Recieving Data from Serial Port!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        Color[] __colors = { Color.Black, Color.Blue, Color.Red, Color.Yellow, Color.Green, Color.DarkMagenta,
                                Color.DarkRed, Color.DeepPink, Color.DarkOliveGreen, Color.DarkKhaki, Color.DarkCyan};

        public string UpdateData(int[] datas)
        {
            // START button was clicked?
            string ret = "";
            for (int i = 0; i < datas.Length; i++) { ret += datas[i] * __unit / __scaler + ", "; }
            if (!bStart)
            {
                return ">> " + ret + "\r\n";
            }

            if ((NumOfPlots == datas.Length) && (NumOfPlots > 0))
            {
                try
                {
                    if (zed.GraphPane.CurveList.Count != NumOfPlots)
                    {
                        ResetGraph();
                        zed.GraphPane.CurveList.Clear();
                        for (int i = 0; i < NumOfPlots; i++)
                        {
                            LineItem curve = zed.GraphPane.AddCurve("Curve " + i, new RollingPointPairList(maxNumOfPointPerAxis + 5), __colors[i % __colors.Length], SymbolType.None);
                            curve.Line.IsSmooth = true;
                            curve.Line.SmoothTension = 0.5F;
                            curve.IsSelectable = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error while Recieving Data from Serial Port!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                try
                {
                    if (zed.GraphPane.CurveList.Count > 0)
                    {
                        for (int i = 0; i < NumOfPlots && i < datas.Length; i++)
                        {
                            zed.GraphPane.CurveList[i].AddPoint(_nX_Index, datas[i] * __unit / __scaler);
                        }
                        _nX_Index += 1;
                        if (zed.GraphPane.CurveList.Count > 0 && zed.GraphPane.CurveList[0].NPts > maxNumOfPointPerAxis)
                        {
                            for (int i = 0; i < NumOfPlots; i++)
                            {
                                zed.GraphPane.CurveList[i].RemovePoint(0);
                            }
                        }
                        return ret + "\r\n";
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error while Recieving Data from Serial Port!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return ret + "\r\n";
        }


        // ham ve voi 2 bien dau vao co gia tri string. 
        // setpoint - tocdo dat
        // current-gia tri hien thoi;
        public void Draw()
        {
            if (zed.GraphPane.CurveList.Count < 1) return;
            Scale xScale = zed.GraphPane.XAxis.Scale;
            double distance = 0;
            LineItem curve_0 = zed.GraphPane.CurveList[0] as LineItem;
            IPointListEdit list_0 = curve_0.Points as IPointListEdit;
            int numPoint_0 = list_0.Count;
            if (numPoint_0 > 10)
            {
                distance = list_0[numPoint_0 - 1].X + xScale.MajorStep;
                if (xScale.Max < distance) { xScale.Max = distance; }
                if (_OneMode == 1) // SROLL mode - NORMAL mode
                {
                    xScale.Min = xScale.Max - xMax;
                }
                else // COMPACT MODE
                {
                    xScale.Min = list_0[0].X;
                }
            }
            zed.AxisChange();
            zed.Invalidate();
        }

        private void DataReceived_Display(string s, string s2)
        {
            if (txtDataReceived.InvokeRequired)
            {
                DlDisplay sd = new DlDisplay(DataReceived_Display);
                txtDataReceived.Invoke(sd, new object[] { s, s2 });
            }
            else
            {
                if (bStart) txtNumOfPlots.Text = s2;

                if (_indLine < NumOfLines)
                {
                    _indLine++;
                }
                else
                {
                    int ind = txtDataReceived.Text.IndexOf('\n') + 1;
                    txtDataReceived.Text = txtDataReceived.Text.Substring(ind);
                }
                txtDataReceived.Text += s;
                // Set cursor at the end of text
                txtDataReceived.SelectionStart = txtDataReceived.Text.Length;
                txtDataReceived.ScrollToCaret();
            }
        }

        private void Terminal_Only_Handler(object sender, EventArgs e)
        {
            if (chk_TerminalOnly.Checked)
            {
                if (timer1.Enabled)
                {
                    timer1.Stop();  // stop timer
                    timer1.Enabled = false; // disable timer
                    bStart = false; // stop updating points to the graph
                }
            }
            else
            {
                if (!timer1.Enabled)
                {
                    bStart = true;
                    timer1.Enabled = true;
                    timer1.Start();
                    for (int i = 0; i < zed.GraphPane.CurveList.Count; i++)
                    {
                        zed.GraphPane.CurveList[i].Clear(); // clear points in the curves
                    }
                }
            }
        }

        private void Send_PID_Handle(object sender, EventArgs e)
        {
            // sendin
            double kP, kI, kD;
            bool ret = double.TryParse(txt_kp.Text, out kP);
            ret &= double.TryParse(txt_ki.Text, out kI);
            ret &= double.TryParse(txt_kd.Text, out kD);
            if (!ret)
            {
                MessageBox.Show("PID Params Incorrect!!!", "Error");
                return;
            }
            byte crc = 0xFF;
            int i = 0;
            byte[] buff; buff = new byte[14];

            buff[i++] = (byte)'$';

            int temp = (int)(kP * 1000);
            buff[i] = (byte)(temp & 0xFF); crc ^= buff[i++];
            buff[i] = (byte)(temp >> 8);   crc ^= buff[i++];
            buff[i] = (byte)(temp >> 16);  crc ^= buff[i++];
            buff[i] = (byte)(temp >> 24);  crc ^= buff[i++];
            temp = (int)(kI * 1000);
            buff[i] = (byte)(temp & 0xFF); crc ^= buff[i++];
            buff[i] = (byte)(temp >> 8);   crc ^= buff[i++];
            buff[i] = (byte)(temp >> 16);  crc ^= buff[i++];
            buff[i] = (byte)(temp >> 24);  crc ^= buff[i++];
            temp = (int)(kD * 1000);
            buff[i] = (byte)(temp & 0xFF); crc ^= buff[i++];
            buff[i] = (byte)(temp >> 8);   crc ^= buff[i++];
            buff[i] = (byte)(temp >> 16);  crc ^= buff[i++];
            buff[i] = (byte)(temp >> 24);  crc ^= buff[i++];
            buff[i] = crc;

            //
            __serialPort.Write(buff, 0, 14);
            // 
        }

        private void Start_Transmitt(object sender, EventArgs e)
        {
            byte[] s = { (byte)'%', (byte)'#' };
            if (button_Start.Text != "Info")
            {
                button_Start.Text = "Info";
                button_End.Enabled = true;
                btn_SendPID.Enabled = true;
                __serialPort.ReadExisting();
                DataMode = false;
                
                __serialPort.Write(s, 0, 2);
            }
            else
            {
                __serialPort.Write(s, 1, 1);
            }
        }

        private void Stop_Transmitt(object sender, EventArgs e)
        {
            button_Start.Text = "Start";
            button_End.Enabled = false;
            btn_SendPID.Enabled = false;

            byte[] s = { (byte)'@' };
            __serialPort.Write(s, 0, 1);

            DataMode = true;
        }
    }
}
